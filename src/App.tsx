import React, { useState } from "react";
import Countdown, { zeroPad } from "react-countdown";
import "./App.css";
import styled from "styled-components";
import useLocalStorage from "react-use-localstorage";
import { GlobalHotKeys } from "react-hotkeys";
import video from "./video.mp4";
import { useEffect } from "react";

const minutes = 45;
const maxAttempts = 5;
const secret = "Kunnen wij jou helpen";

enum GameState {
  Initial = "Initial",
  Running = "Running",
  HintShown = "HintShown",
  GameOver = "GameOver",
}

const failedAttemptsKey = "FailedAttempts";

function App() {
  const [gameState, setGameState] = useLocalStorage(
    "GameState",
    GameState.Initial
  );
  const [failedAttempts, setFailedAttempts] = useState<number>(0);
  const [consoleInput, setConsoleInput] = useState<string>("");
  const [displayMessage, setDisplayMessage] = useState<string>("");
  const [isProcessing, setIsProcessing] = useState<boolean>(false);
  const [countdownTarget, setCountdownTarget] =
    useLocalStorage("countdownTarget");
  const HintVideo = () => {
    return <video src={video} autoPlay />;
  };

  useEffect(() => {
    const localStorageFailedAttempts = localStorage.getItem(failedAttemptsKey);
    if (localStorageFailedAttempts !== null) {
      setFailedAttempts(parseInt(localStorageFailedAttempts));
    }
  }, []);

  useEffect(() => {
    if (failedAttempts) {
      localStorage.setItem(failedAttemptsKey, failedAttempts.toString());
    }
  }, [failedAttempts]);

  const handleStart = () => {
    setCountdownTarget(
      new Date(new Date().getTime() + minutes * 60000 + 1000).toString()
    );
    setGameState(GameState.Running);
  };

  const handleClick = () => {
    setIsProcessing(true);
    setDisplayMessage("Wachten op antwoord...");
    setTimeout(evaluateInput, 3000);
  };

  const evaluateInput = () => {
    if (consoleInput.toLowerCase().includes(secret.toLowerCase())) {
      loadHint();
    } else {
      const newFailedAttempts = failedAttempts + 1;
      if (newFailedAttempts >= maxAttempts) {
        setDisplayMessage("Game Over");
        setGameState(GameState.GameOver);
      } else {
        setIsProcessing(false);
        setDisplayMessage("Niemand gaf gehoor. Probeer het opnieuw!");
      }
      setFailedAttempts(newFailedAttempts);
    }
  };

  const loadHint = () => {
    setTimeout(() => {
      setDisplayMessage(
        "Wachten op antwoord...\rDe planchette op het Ouija-bord begint te bewegen."
      );
      setTimeout(() => {
        setDisplayMessage(
          "Wachten op antwoord...\rDe planchette op het Ouija-bord begint te bewegen.\rEn stopt op het woord 'JA'"
        );
        setTimeout(() => {
          setDisplayMessage(
            "Wachten op antwoord...\rDe planchette op het Ouija-bord begint te bewegen.\rEn stopt op het woord 'JA'\r" +
              "En er volgt meer...\r"
          );
          setTimeout(() => setGameState(GameState.HintShown), 2000);
        }, 2000);
      }, 2000);
    }, 2000);
  };

  const reset = () => {
    setGameState(GameState.Initial);
    setFailedAttempts(0);
    localStorage.removeItem(failedAttemptsKey);
    setDisplayMessage("");
    setConsoleInput("");
    setIsProcessing(false);
  };

  const keyMap = {
    RESET: "shift+Q+R",
  };
  const handlers = {
    RESET: () => reset(),
  };

  return (
    <div className="App">
      <GlobalHotKeys keyMap={keyMap} handlers={handlers} />;
      <StyledTopWrapper>
        {gameState === GameState.Initial && (
          <StyledButton onClick={() => handleStart()}>Start</StyledButton>
        )}
        {(gameState === GameState.Running ||
          gameState === GameState.HintShown) && (
          <StyledCountDown>
            <Countdown
              autoStart={true}
              date={countdownTarget}
              renderer={(props) => {
                if (props.completed) {
                  return <span>Out of time</span>;
                } else {
                  return (
                    <span>
                      {zeroPad(props.hours)}:{zeroPad(props.minutes)}:
                      {zeroPad(props.seconds)}
                    </span>
                  );
                }
              }}
            />
          </StyledCountDown>
        )}
        {gameState === GameState.GameOver && (
          <StyledTopMessage>Game Over</StyledTopMessage>
        )}
      </StyledTopWrapper>
      {gameState === GameState.HintShown && (
        <StyledHintWrapper>
          <HintVideo />
        </StyledHintWrapper>
      )}
      {gameState === GameState.Running && (
        <StyledConsole>
          <StyledInputWrapper>
            <StyledInput
              type="text"
              placeholder="Welke vraag wil je stellen?"
              disabled={isProcessing}
              onChange={(event) => setConsoleInput(event.target.value)}
            />
            <StyledNumberOfAttempts>
              {failedAttempts}/{maxAttempts}
            </StyledNumberOfAttempts>
            <StyledButton onClick={() => handleClick()} disabled={isProcessing}>
              Enter
            </StyledButton>
          </StyledInputWrapper>
          <StyledTextArea value={displayMessage} spellCheck={false} readOnly />
        </StyledConsole>
      )}
    </div>
  );
}

const StyledTopWrapper = styled.div`
  height: 100%;
  display: flex;
  align-items: center;
  justify-content: center;
`;

const StyledCountDown = styled.div`
  font-family: "Share Tech Mono", monospace;
  font-size: 60px;
  color: #da9742;
`;

const StyledTopMessage = styled.h1`
  font-family: "Share Tech Mono", monospace;
  font-size: 160px;
  color: #da9742;
`;

const StyledConsole = styled.div`
  width: 800px;
  height: 500px;
  margin: 20px auto;
  border: 3px solid #da9742;
  border-radius: 5px;
  padding: 10px;
  display: grid;
  grid-template-rows: [inputWrapper] auto [textArea] 1fr;
  grid-gap: 10px;
`;

const StyledInputWrapper = styled.div`
  grid-area: "inputWrapper";
  display: grid;
  grid-template-columns: [input] auto [attempts] 60px [submit] 180px;
  grid-gap: 10px;
`;

const StyledInput = styled.input`
  grid-area: "input";
  border: 3px solid #da9742;
  font-family: "Share Tech Mono", monospace;
  font-size: 30px;
  padding: 10px;
  color: #da9742;
  background-color: transparent;
  border-radius: 5px;

  &:focus {
    outline: none;
  }
`;

const StyledNumberOfAttempts = styled.div`
  grid-area: "attempts";
  color: #da9742;
  font-size: 30px;
  font-family: "Share Tech Mono", monospace;
  align-self: center;
`;

const StyledButton = styled.button`
  font-family: "Share Tech Mono", monospace;
  font-size: 30px;
  padding: 10px 20px;
  border: 3px solid #da9742;
  color: #da9742;
  background-color: transparent;
  border-radius: 5px;
  font-weight: bold;

  &:hover {
    background-color: #63431a;
    cursor: pointer;
  }
`;

const StyledHintWrapper = styled.div`
  border: 3px solid #da9742;
  font-family: "Share Tech Mono", monospace;
  font-size: 30px;
  padding: 10px;
  color: #da9742;
  background-color: transparent;
  border-radius: 5px;
  max-height: 800px;
  max-width: 1000px;
  margin: 0 auto;
`;

const StyledTextArea = styled.textarea`
  border: 3px solid #da9742;
  font-family: "Share Tech Mono", monospace;
  font-size: 24px;
  padding: 10px;
  color: #da9742;
  background-color: transparent;
  border-radius: 5px;
  resize: none;

  &:focus {
    outline: none;
  }
`;

export default App;
